app.controller('productsController', ['$rootScope','$scope', '$state', 'productModel','$timeout', function($rootScope,$scope, $state, productModel,$timeout){
	
	angular.extend($scope, {
		data:{},
		editData:{},
		invalidForm : false,
	});
	$scope.products = [{
		"product_id": 1,
		"product_name": "Air Conditionar",
	},{
		"product_id": 2,
		"product_name": "Laptop",
	}];
	$timeout(function(){
		//$scope.getProductsList();
		
	}, 0);
	angular.extend($scope, {
		/*getProductsList: function(){
			
	         productModel.getProducts().then(function(response){
	         	if(response.data.status_code == 200){
	         		$scope.products = response.data.result;
	         		
	         	}else{
	         		$scope.products = [];
	         	}
	         });     
        },*/
        AddProduct : function() {
        	
        	$timeout(function() {
        		angular.element('.add-product').modal({backdrop: 'static', keyboard: false});
			}, 100);
        },
        addProductDetail : function(form) {
        	if(form.$valid){
        		productModel.addProduct($scope.data).then(function(response){
					if(response.data.status_code == 200)
					{
						
                    	$state.go('products',{},{reload :true});
					}
				});
        	}else{
        		$scope.invalidForm = true;
        	}
        },
        editProductDetail : function(form) {
        	if(form.$valid){
        		productModel.editProduct($scope.editData).then(function(response){
					if(response.data.status_code == 200)
					{
						
                    	$state.go('products',{},{reload :true});
					}
				});
        	}else{
        		$scope.invalidForm = true;
        	}
        },
        editProduct : function(edit) {
        	$scope.editData = angular.copy(edit);
			//console.log($scope.editData);
			angular.element(".edit-product").modal({backdrop: 'static', keyboard: false});
        },
		addtoCart : function(product_id) {
			productModel.addtoCart(product_id).then(function(response){
	         	if(response.data.status_code == 200){
	         		$state.go('products',{},{reload :true});
	         		
	         	}
	         });     
		},

	});
}]);