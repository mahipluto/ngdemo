app.factory('loginModel', function($http){

	var baseUrl = "http://localhost:8000/";
	var loginModel = {};
	
	loginModel.login = function(data){
		return 	$http({
				url:baseUrl+'login',
				method: 'POST',
				data: data
			});
	}

	return loginModel;
});
