var app = angular.module('demoApp', ['ui.router']);

app.run(['$rootScope', '$state', function ($rootScope, $state){
    $rootScope.$on('$stateChangeStart',
            function (event, toState, toParams, fromState, fromParams,$state) {

               console.log('changing');
               	/*if(toState.url=='/login'){
		            if (sessionStorage.getItem("sessionid") != null || sessionStorage.getItem("sessionid") != '' || sessionStorage.getItem("sessionid") != undefined) 
	                {
	                   $state.go('products',{},{reload :true});
	                } 
		        }*/
           
	});
}]);
app.config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/login');

    $stateProvider
	    .state('login', {
	        url: '/login',
	        templateUrl: 'login.html',
	        controller: 'loginController'
	        
	    })
	    .state('products', {
	        url: '/products',
	        templateUrl: 'products.html',
	        controller: 'productsController'
	        
	    })
 });