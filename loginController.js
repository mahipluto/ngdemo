app.controller('loginController', ['$rootScope','$scope', '$state', 'loginModel', function($rootScope,$scope, $state, loginModel){
	
	angular.extend($scope, {
		data:{},
		invalidForm : false,
	});

	angular.extend($scope, {
		doLogin: function(form){
						
			if(form.$valid){
				
				loginModel.login($scope.data).then(function(response){
					if(response.data.status == 200)
					{
						sessionStorage.setItem('sessionid', response.sessionid);
                    	localStorage.setItem('userid', response.userid);
                    	$state.go('products',{},{reload :true});
					}
				});
			}else{
				$scope.invalidForm = true;
			}
		},
		
		
		
	});
}]);