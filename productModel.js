app.factory('productModel', function($http){

	var baseUrl = "http://localhost:8000/";
	var productModel = {};
	
	productModel.getProducts = function(data){
		return 	$http({
				url:baseUrl+'get-products',
				method: 'GET'
				
			});
	}

	productModel.addProduct = function(data){
		var request = {
        	product_name : data.product_name
        };
        
        var fd = new FormData();
        fd.append('request', JSON.stringify(request));
        fd.append('logo', data.logo);

        return $http({
            method: "POST",
            url: baseUrl + 'add-product',
            data: fd,
            headers: {'Content-Type': undefined}
        });
		
	}

	productModel.editProduct = function(data){
		var request = {
        	product_name : data.product_name,
        	product_id : data.product_id
        };
        
        var fd = new FormData();
        fd.append('request', JSON.stringify(request));
        fd.append('logo', data.logo);

        return $http({
            method: "POST",
            url: baseUrl + 'edit-product',
            data: fd,
            headers: {'Content-Type': undefined}
        });
		
	}

	productModel.addtoCart = function(product_id){
		return 	$http({
				url:baseUrl+'add-to-cart',
				method: 'POST',
				data : {'product_id' : product_id}
			});
	}

	return productModel;
});
